# Change log of [`gurisitx.dedibox`collection](https://galaxy.ansible.com/gurisitx/dedibox)


## [v1.1.1](https://framagit.org/gurisitx/dedibox/-/tags/v1.1.1)

* migration to gurisitx namespace
* three roles to configure a Proxmox cluster


## [<v1.1.1](https://galaxy.ansible.com/ulvida/dedibox)

* initial versions under personal namespace.
