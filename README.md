# Ansible Collection - gurisitx.dedibox

Some roles to configure dedibox bare metal servers proposed by Scaleway / online.net, according to their documentartion. Presently, collection's roles are: 

* network: IPv4 and IPv6 network configuration of a dedibox/online/scaleway bare metal server
* proxmox: Proxmox VE installation on a debian
* rpn_san: configuration of online/scaleway RPN SAN service
