Dedibox / online /Scaleway iSCSI RPN SAN configuration
=========

The role installs and configures open-iscsi [as documented here](https://www.scaleway.com/en/docs/dedibox-network/rpn/how-to/mount-rpn-san-linux/), for an dedbibox server to access a storage volume in the SAN private network.  

Requirements
------------

A deibox server with a linux distribution installed and an RPN SAN volume. 

Role Variables
--------------

This role has no variable

Dependencies
------------

No dependencies

Example Playbook
----------------

Just call: 

```yaml
    - hosts: dedibox
      roles:
         - role: ulvida.dedibox_rpn
```

License
-------

GPLv3

Author Information
------------------

Daniel Viñar Ulriksen (aka @ulvida)
